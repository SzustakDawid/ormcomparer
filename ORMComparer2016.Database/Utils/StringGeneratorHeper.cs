﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Utils
{
    public static class StringGeneratorHelper
    {
        private static Random random = new Random();

        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string RandomString(int minValue, int maxValue)
        {
            int size = random.Next(minValue, maxValue);
            return RandomString(size);
        }
    }
}
