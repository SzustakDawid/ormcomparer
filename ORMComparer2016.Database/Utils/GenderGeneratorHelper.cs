﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Utils
{
    public class GenderGeneratorHelper
    {
        private static Random random = new Random();

        public static string RandomGender()
        {
            int value = random.Next(1, 10);

            string gender = "F";

            if (value % 2 == 0)
                gender = "M";

            return gender;
        }
    }
}
