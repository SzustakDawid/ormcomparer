﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Utils
{
    public class DateGeneratorHelper
    {
        private static Random random = new Random();

        public static DateTime RandomDate(int minDate = 1960, int maxDate = 2100)
        {
            if (minDate < 1960)
                minDate = 1960;

            if (maxDate > 2100)
                maxDate = 2100;

            int year = random.Next(minDate, maxDate);
            int month = random.Next(1, 12);
            int day = random.Next(1, 28);

            return new DateTime(year, month, day);
        }
    }
}
