﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Utils
{
    public class ConnectionString
    {
        public const string SQLServer = "SQLServerConnection";

        public const string MySQL = "MySQLConnection";
    }
}
