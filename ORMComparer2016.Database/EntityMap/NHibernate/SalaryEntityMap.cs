﻿using FluentNHibernate.Mapping;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate
{
    public class SalaryEntityMap : ClassMap<SalaryEntity>
    {
        public SalaryEntityMap()
        {
            Table("Salaries");

            Id(x => x.Id)
                .GeneratedBy
                .Identity();

            Map(x => x.Salary)
                .Precision(9)
                .Scale(2);

            Map(x => x.FromDate);
            Map(x => x.ToDate);

            References(x => x.Employee);
        }
    }
}
