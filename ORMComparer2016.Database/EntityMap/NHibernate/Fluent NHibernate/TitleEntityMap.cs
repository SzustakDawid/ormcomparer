﻿using FluentNHibernate.Mapping;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate
{
    public class TitleEntityMap : ClassMap<TitleEntity>
    {
        public TitleEntityMap()
        {
            Table("Titles");

            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Title);
        }
    }
}
