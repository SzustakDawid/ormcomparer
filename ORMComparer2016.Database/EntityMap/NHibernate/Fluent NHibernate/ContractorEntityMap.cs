﻿using FluentNHibernate.Mapping;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate
{
    public class ContractorEntityMap : ClassMap<ContractorEntity>
    {
        public ContractorEntityMap()
        {
            Table("Contractors");

            Id(x => x.Id)
                .GeneratedBy
                .Identity();

            Map(x => x.Acronym);
            Map(x => x.AdditionalInformation);
            Map(x => x.City);
            Map(x => x.Country);
            Map(x => x.CreatedAt);
            Map(x => x.Fax);
            Map(x => x.LongName);
            Map(x => x.Mail);
            Map(x => x.MobilePhone);
            Map(x => x.Name);
            Map(x => x.Nip);
            Map(x => x.PhoneNumber1);
            Map(x => x.PhoneNumber2);
            Map(x => x.Postcode);
            Map(x => x.Region);
            Map(x => x.Regon);
            Map(x => x.SecondMail);
            Map(x => x.Street);
            Map(x => x.Website);
        }
    }
}