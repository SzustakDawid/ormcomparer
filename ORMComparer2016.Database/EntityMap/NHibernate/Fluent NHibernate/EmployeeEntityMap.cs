﻿using FluentNHibernate.Mapping;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate
{
    public class EmployeeEntityMap : ClassMap<EmployeeEntity>
    {
        public EmployeeEntityMap()
        {
            Table("Employees");

            Id(x => x.Id)
              .GeneratedBy
              .Identity();

            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.Gender);
            Map(x => x.BirthDate);
            Map(x => x.HireDate);

            HasOne(x => x.Title)
                .Cascade.All();

            HasMany(x => x.Salaries);

            HasManyToMany(x => x.Departments);
        }
    }
}
