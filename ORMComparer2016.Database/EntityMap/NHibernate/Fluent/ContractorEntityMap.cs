﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate.Fluent
{
    public class ContractorsMap : ClassMapping<ContractorEntity>
    {
        public ContractorsMap()
        {
            Table("Contractors");
            Schema("dbo");
            Lazy(true);

            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Acronym);
            Property(x => x.Name);
            Property(x => x.LongName);
            Property(x => x.Postcode);
            Property(x => x.City);
            Property(x => x.Street);
            Property(x => x.PhoneNumber1);
            Property(x => x.PhoneNumber2);
            Property(x => x.MobilePhone);
            Property(x => x.Fax);
            Property(x => x.Mail);
            Property(x => x.SecondMail);
            Property(x => x.Website);
            Property(x => x.Nip);
            Property(x => x.Regon);
            Property(x => x.Region);
            Property(x => x.AdditionalInformation);
            Property(x => x.Country);
            Property(x => x.CreatedAt, map => map.NotNullable(true));
        }
    }
}
