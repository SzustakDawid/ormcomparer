﻿using FluentNHibernate.Mapping;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.EntityMap.NHibernate
{
    public class DepartmentEntityMap : ClassMap<DepartmentEntity>
    {
        public DepartmentEntityMap()
        {
            Table("Departments");

            Id(x => x.Id)
                .GeneratedBy
                .Identity();

            Map(x => x.Name);

            HasManyToMany(x => x.Employees);
        }
    }
}
