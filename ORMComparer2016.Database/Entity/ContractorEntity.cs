﻿using ORMComparer2016.Database.Base;
using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Entity
{
    public class ContractorEntity
    {
        public virtual int Id { get; set; }
        public virtual string Acronym { get; set; }
        public virtual string Name { get; set; }
        public virtual string LongName { get; set; }
        public virtual string Postcode { get; set; }
        public virtual string City { get; set; }
        public virtual string Street { get; set; }
        public virtual string PhoneNumber1 { get; set; }
        public virtual string PhoneNumber2 { get; set; }
        public virtual string MobilePhone { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Mail { get; set; }
        public virtual string SecondMail { get; set; }
        public virtual string Website { get; set; }
        public virtual string Nip { get; set; }
        public virtual string Regon { get; set; }
        public virtual string Region { get; set; }
        public virtual string AdditionalInformation { get; set; }
        public virtual string Country { get; set; }
        public virtual DateTime CreatedAt { get; set; }


        #region Utils

        public static ContractorEntity GenerateInstance()
        {
            return new ContractorEntity()
            {
                Acronym = StringGeneratorHelper.RandomString(5),
                AdditionalInformation = StringGeneratorHelper.RandomString(0, 100),
                City = StringGeneratorHelper.RandomString(4, 10),
                Country = StringGeneratorHelper.RandomString(4, 10),
                CreatedAt = DateTime.Now,
                Fax = StringGeneratorHelper.RandomString(9),
                LongName = StringGeneratorHelper.RandomString(4, 30),
                Mail = StringGeneratorHelper.RandomString(5, 30),
                MobilePhone = StringGeneratorHelper.RandomString(9),
                Name = StringGeneratorHelper.RandomString(4, 10),
                Nip = StringGeneratorHelper.RandomString(10),
                PhoneNumber1 = StringGeneratorHelper.RandomString(9),
                PhoneNumber2 = StringGeneratorHelper.RandomString(9),
                Postcode = StringGeneratorHelper.RandomString(5),
                Region = StringGeneratorHelper.RandomString(5, 15),
                Regon = StringGeneratorHelper.RandomString(9),
                SecondMail = StringGeneratorHelper.RandomString(5, 30),
                Street = StringGeneratorHelper.RandomString(9, 40),
                Website = StringGeneratorHelper.RandomString(5, 30)
            };
        }

        public static List<ContractorEntity> GenerateInstances(int count)
        {
            List<ContractorEntity> listToReturn = new List<ContractorEntity>();

            for (int i = 0; i < count; i++)
                listToReturn.Add(GenerateInstance());

            return listToReturn;
        }

        public static ContractorEntity CloneInstance(ContractorEntity contractor)
        {
            return new ContractorEntity()
            {
                Id = contractor.Id,
                Acronym = contractor.Acronym,
                AdditionalInformation = contractor.AdditionalInformation,
                City = contractor.City,
                Country = contractor.Country,
                CreatedAt = contractor.CreatedAt,
                Fax = contractor.Fax,
                LongName = contractor.LongName,
                Mail = contractor.Mail,
                MobilePhone = contractor.MobilePhone,
                Name = contractor.Name,
                Nip = contractor.Nip,
                PhoneNumber1 = contractor.PhoneNumber1,
                PhoneNumber2 = contractor.PhoneNumber2,
                Postcode = contractor.Postcode,
                Region = contractor.Region,
                Regon = contractor.Regon,
                SecondMail = contractor.SecondMail,
                Street = contractor.Street,
                Website = contractor.Website
            };
        }

        public static List<ContractorEntity> CloneInstances(List<ContractorEntity> contractorEntityList)
        {
            var result = new List<ContractorEntity>();

            foreach (var contractor in contractorEntityList)
            {
                result.Add(CloneInstance(contractor));
            }

            return result;
        }
        #endregion
    }
}
