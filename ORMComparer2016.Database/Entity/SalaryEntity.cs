﻿using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Entity
{
    public class SalaryEntity 
    {
        public virtual int Id { get; set; }

        public virtual EmployeeEntity Employee { get; set; }

        public virtual decimal Salary { get; set; }

        public virtual DateTime FromDate { get; set; }

        public virtual DateTime ToDate { get; set; }

        #region Utils

        public static SalaryEntity GenerateInstance(EmployeeEntity emp)
        {
            return new SalaryEntity()
            {
                Employee = emp,
                FromDate = DateGeneratorHelper.RandomDate(),
                ToDate = DateGeneratorHelper.RandomDate(),
                Salary = DecimalGeneratorHelper.RandomDecimal(),
            };
        }

        public static IList<SalaryEntity> GenerateInstances(EmployeeEntity emp)
        {
            List<SalaryEntity> result = new List<SalaryEntity>();

            int count = new Random(DateTime.Now.Millisecond).Next(1,5);

            for (int i = 0 ; i < count ; i++)
            {
                result.Add(GenerateInstance(emp));
            }

            return result;
        }

        public static SalaryEntity CloneInstance(EmployeeEntity emp, SalaryEntity salary)
        {
            return new SalaryEntity()
            {
                Employee = emp,
                FromDate = salary.FromDate,
                ToDate = salary.ToDate,
                Salary = salary.Salary
            };
        }

        public static IList<SalaryEntity> CloneInstances(EmployeeEntity emp, IList<SalaryEntity> salaries)
        {
            List<SalaryEntity> result = new List<SalaryEntity>();
            
            foreach(var el in salaries)
            {
                result.Add(CloneInstance(emp, el));
            }

            return result;
        }

        #endregion Utils
    }
}
