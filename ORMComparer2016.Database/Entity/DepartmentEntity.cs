﻿using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Entity
{
    public class DepartmentEntity
    {
        public DepartmentEntity()
        {
            Employees = new List<EmployeeEntity>();
        }

        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<EmployeeEntity> Employees { get; set; }

        #region Utils

        public static DepartmentEntity GenerateInstance()
        {
            return new DepartmentEntity()
            {
                Name = StringGeneratorHelper.RandomString(4, 20),
            };
        }

        public static DepartmentEntity GenerateExistInstance()
        {
            return new DepartmentEntity();
        }

        public static List<DepartmentEntity> GenerateInstances(int count)
        {
            var result = new List<DepartmentEntity>();

            for (int i = 0; i < count; i++)
                result.Add(GenerateInstance());

            return result;
        }

        public static List<DepartmentEntity> GenerateExistInstances(int count)
        {
            var result = new List<DepartmentEntity>();

            for (int i = 0; i < count; i++)
            {
                var obj = GenerateExistInstance();
                if (!result.Any(x => x.Id == obj.Id))
                    result.Add(obj);
            }
               
            return result;
        }

        public static DepartmentEntity CloneInstance(DepartmentEntity department)
        {
            return new DepartmentEntity()
            {
                Id = department.Id,
                Name = department.Name
            };
        }

        public static List<DepartmentEntity> CloneInstances(List<DepartmentEntity> departmentEntityList)
        {
            var result = new List<DepartmentEntity>();

            foreach (var department in departmentEntityList)
            {
                result.Add(CloneInstance(department));
            }

            return result;
        }
        #endregion
    }
}
