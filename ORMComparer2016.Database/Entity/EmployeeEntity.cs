﻿using NHibernate.Util;
using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Entity
{
    public class EmployeeEntity
    {
        public EmployeeEntity()
        {
            Salaries = new List<SalaryEntity>();
            Departments = new List<DepartmentEntity>();
        }

        public virtual int Id { get; set; }

        public virtual DateTime BirthDate { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Gender { get; set; }

        public virtual DateTime HireDate { get; set; }

        public virtual TitleEntity Title { get; set; }

        public virtual IList<SalaryEntity> Salaries { get; set; }

        public virtual IList<DepartmentEntity> Departments { get; set; }

        #region Utils

        public static EmployeeEntity GenerateInstance()
        {
            var employee = new EmployeeEntity()
            {
                BirthDate = DateGeneratorHelper.RandomDate(),
                FirstName = StringGeneratorHelper.RandomString(20),
                LastName = StringGeneratorHelper.RandomString(20),
                Gender = GenderGeneratorHelper.RandomGender(),
                HireDate = DateGeneratorHelper.RandomDate(),

                Title = TitleEntity.GenerateInstance(),
            };

            employee.Salaries = SalaryEntity.GenerateInstances(employee);

            return employee;
        }

        public static List<EmployeeEntity> GenerateInstances(int count)
        {
            var result = new List<EmployeeEntity>();

            for (int i = 0; i < count; i++)
                result.Add(GenerateInstance());

            return result;
        }

        public static EmployeeEntity CloneInstance(EmployeeEntity employee)
        {
            var emp = new EmployeeEntity()
            {
                BirthDate = employee.BirthDate,
                FirstName = employee.FirstName,
                Gender = employee.Gender,
                HireDate = employee.HireDate,
                LastName = employee.LastName,

                Title = TitleEntity.CloneInstance(employee.Title),
                
            };
            emp.Departments = DepartmentEntity.CloneInstances(employee.Departments.ToList());
            emp.Salaries = SalaryEntity.CloneInstances(emp, employee.Salaries);

            return emp;
        }

        public static List<EmployeeEntity> CloneInstances(List<EmployeeEntity> employeeEntityList)
        {
            var result = new List<EmployeeEntity>();

            foreach (var employee in employeeEntityList)
            {
                result.Add(CloneInstance(employee));
            }

            return result;
        }
        #endregion
    }
}
