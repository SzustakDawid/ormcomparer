﻿using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Entity
{
    public class TitleEntity
    {
        public virtual int Id { get; set; }

        public virtual string Title { get; set; }

        #region Utils

        public static TitleEntity GenerateInstance()
        {
            return new TitleEntity()
            {
                Title = StringGeneratorHelper.RandomString(4, 20),
            };
        }

        public static TitleEntity CloneInstance(TitleEntity title)
        {
            return new TitleEntity()
            {
                Title = title.Title
            };
        }
        #endregion
    }
}
