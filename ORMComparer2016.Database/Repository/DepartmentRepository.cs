﻿using NHibernate.Linq;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Repository
{
    public class DepartmentRepository
    {
        public static void Initialize(int value = 100)
        {
            List<DepartmentEntity> _departmentsToSave = null;

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (session.Query<DepartmentEntity>().Count() == 0)
                    {
                        _departmentsToSave = DepartmentEntity.GenerateInstances(value);

                        var departmentsToSave = DepartmentEntity.CloneInstances(_departmentsToSave);

                        foreach (var el in departmentsToSave)
                        {
                            session.Save(el);
                        }

                        transaction.Commit();
                    }
                }
            }

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (session.Query<DepartmentEntity>().Count() == 0)
                    {
                        var departmentsToSave = DepartmentEntity.CloneInstances(_departmentsToSave);

                        foreach (var el in departmentsToSave)
                        {
                            session.Save(el);
                        }

                        transaction.Commit();
                    }
                }
            }
        }
    }
}
