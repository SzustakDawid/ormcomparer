﻿using ORMComparer2016.Database.Entity;
using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Config.Entity_Framework
{
    public static class EntityFrameworkContextFactory
    {
        public static bool IsSQLServerConnectionCorrect { get; set; }

        public static bool IsMySQLConnectionCorrect { get; set; }

        public static DatabaseContext GetSQLServerContext()
        {
            try
            {
                return new DatabaseContext(ConnectionString.SQLServer);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DatabaseContext GetMySQLContext()
        {
            try
            {
                return new DatabaseContext(ConnectionString.MySQL);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void ConfigureEntityFramework(bool alwaysDropDatabase = false)
        {
            using (var sqlContext = GetMySQLContext())
            {
                try
                {
                    if (alwaysDropDatabase)
                    {
                        new DropCreateDatabaseAlways<DatabaseContext>()
                            .InitializeDatabase(sqlContext);
                    }

                    sqlContext.Database.CommandTimeout = 3;
                    sqlContext.Database.Connection.Open();
                    sqlContext.Contractors.Take<ContractorEntity>(1);

                    IsMySQLConnectionCorrect = true;
                }
                catch (Exception)
                {
                    IsMySQLConnectionCorrect = false;
                }
            }

            using (var sqlContext = GetSQLServerContext())
            {
                try
                {
                    if (alwaysDropDatabase)
                    {
                        new DropCreateDatabaseAlways<DatabaseContext>()
                            .InitializeDatabase(sqlContext);
                    }

                    sqlContext.Database.CommandTimeout = 3;
                    sqlContext.Database.Connection.Open();
                    sqlContext.Contractors.Take<ContractorEntity>(1);

                    IsSQLServerConnectionCorrect = true;
                }
                catch (Exception)
                {
                    IsMySQLConnectionCorrect = false;
                }
            }
        }
    }
}
