﻿using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Config.Entity_Framework
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(string connectionString)
            : base(connectionString)
        { }

        public virtual DbSet<ContractorEntity> Contractors { get; set; }

        public virtual DbSet<EmployeeEntity> Employees { get; set; }

        public virtual DbSet<DepartmentEntity> Departments { get; set; }

        public virtual DbSet<SalaryEntity> Salaries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContractorEntity>().ToTable("Contractors");
            modelBuilder.Entity<SalaryEntity>().ToTable("Salaries");
            modelBuilder.Entity<TitleEntity>().ToTable("Titles");

            var emp = modelBuilder.Entity<EmployeeEntity>().ToTable("Employees");

            emp.HasMany(z => z.Salaries)
                .WithRequired(x => x.Employee)
                .Map(e => e.MapKey("Employee_Id"))
                .WillCascadeOnDelete(true);

            emp.HasMany(d => d.Departments)
                .WithMany(e => e.Employees)
                .Map(x =>
                {
                    x.MapLeftKey("EmployeeEntity_id");
                    x.MapRightKey("DepartmentEntity_id");
                    x.ToTable("EmployeesToDepartments");
                });

            modelBuilder.Entity<DepartmentEntity>()
                .ToTable("Departments");

            base.OnModelCreating(modelBuilder);
        }
    }
}
