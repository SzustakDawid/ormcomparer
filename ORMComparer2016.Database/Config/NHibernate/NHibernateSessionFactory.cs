﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Tool.hbm2ddl;
using ORMComparer2016.Database.Base;
using ORMComparer2016.Database.Utils;
using System;
using NHibernate.Cfg;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.AdoNet;
using ORMComparer2016.Database.EntityMap.NHibernate;
using ORMComparer2016.Database.Repository;

namespace ORMComparer2016.Database.Config.NHibernate
{
    public static class NHibernateSessionFactory
    {
        public static ISessionFactory SQLServerSessionFactory { get; set; }

        public static ISessionFactory MySQLSessionFactory { get; set; }

        private static string _conString = "";

        private static bool _createSchema;

        public static void ConfigureNHibernate(bool createSchema = true)
        {
            _createSchema = createSchema;

            if (SQLServerSessionFactory != null)
            {
                SQLServerSessionFactory.Close();
                SQLServerSessionFactory.Dispose();
            }

            if (MySQLSessionFactory != null)
            {
                MySQLSessionFactory.Close();
                MySQLSessionFactory.Dispose();
            }

            MySQLSessionFactory = null;
            SQLServerSessionFactory = null;
            MySQLSessionFactory = null;

            try
            {
                _conString = ConnectionString.SQLServer;
                var configuration = Fluently.Configure()
                 .Database(SetupDatabase(ConnectionString.SQLServer))
                 .Mappings(z =>
                    z.FluentMappings
                        .Add<ContractorEntityMap>()
                        .Add<EmployeeEntityMap>()
                        .Add<DepartmentEntityMap>()
                        .Add<SalaryEntityMap>()
                        .Add<TitleEntityMap>()
                    )
                 .ExposeConfiguration(ConfigurePersistence)
                 .BuildConfiguration();

                SQLServerSessionFactory = configuration.BuildSessionFactory();
            }
            catch (Exception)
            { }

            try
            {
                _conString = ConnectionString.MySQL;
                var mySQLconfiguration = Fluently.Configure()
                 .Database(SetupDatabase(ConnectionString.MySQL))
                 .Mappings(z => z.FluentMappings
                        .Add<ContractorEntityMap>()
                        .Add<EmployeeEntityMap>()
                        .Add<DepartmentEntityMap>()
                        .Add<SalaryEntityMap>()
                        .Add<TitleEntityMap>()
                    )
                 .ExposeConfiguration(ConfigurePersistence)
                 .BuildConfiguration();

                MySQLSessionFactory = mySQLconfiguration.BuildSessionFactory();
            }
            catch (Exception)
            { }

            try
            {
                DepartmentRepository.Initialize();
            }
            catch (Exception)
            { }
        }

        private static IPersistenceConfigurer SetupDatabase(string connectionString)
        {
            if (connectionString == ConnectionString.SQLServer)
            {
                return MsSqlConfiguration.MsSql2012
                        .Dialect<MsSql2012Dialect>()
                        .Driver<Sql2008ClientDriver>()
                        .ConnectionString(x => x.FromConnectionStringWithKey(connectionString));
            }
            else
            {
                return MySQLConfiguration.Standard
                      .Dialect<MySQL5Dialect>()
                      .Driver<MySqlDataDriver>()
                      .ConnectionString(x => x.FromConnectionStringWithKey(connectionString));
            }
        }

        private static void ConfigurePersistence(Configuration config)
        {
            try
            {
                new SchemaUpdate(config).Execute(false, _createSchema);
                SchemaMetadataUpdater.QuoteTableAndColumns(config);
            }
            catch (Exception)
            { }
        }
    }
}
