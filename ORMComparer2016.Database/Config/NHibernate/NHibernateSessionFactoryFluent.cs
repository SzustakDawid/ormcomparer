﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.Database.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ORMComparer2016.Database.Config.NHibernate
{
    public class NHibernateSessionFactory
    {
        public static ISessionFactory SQLServerSessionFactory { get; set; }

        public static ISessionFactory MySQLSessionFactory { get; set; }

        public static string ConString { get; set; }

        public static void ConfigureNHibernate()
        {
            if (SQLServerSessionFactory != null)
            {
                SQLServerSessionFactory.Close();
                SQLServerSessionFactory.Dispose();
            }

            if (MySQLSessionFactory != null)
            {
                MySQLSessionFactory.Close();
                MySQLSessionFactory.Dispose();
            }

            try
            {
                ConString = ConnectionString.SQLServer;
                var configuration = new Configuration().DataBaseIntegration(db =>
                {
                    db.ConnectionStringName = ConString;
                    db.Dialect<MsSql2012Dialect>();
                    db.Driver<Sql2008ClientDriver>();
                });

                var mapper = new ModelMapper();
                mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());

                HbmMapping mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
                configuration.AddMapping(mapping);

                new SchemaUpdate(configuration).Execute(false, true);
                SchemaMetadataUpdater.QuoteTableAndColumns(configuration);

                SQLServerSessionFactory = configuration.BuildSessionFactory();
            }
            catch (Exception ex)
            { }

            try
            {
                ConString = ConnectionString.MySQL;
                var configuration = new Configuration().DataBaseIntegration(db =>
                {
                    db.ConnectionStringName = ConString;
                    db.Dialect<MySQL55Dialect>();
                    db.Driver<MySqlDataDriver>();
                });

                var mapper = new ModelMapper();
                mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());

                HbmMapping mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
                configuration.AddMapping(mapping);

                new SchemaUpdate(configuration).Execute(false, true);
                SchemaMetadataUpdater.QuoteTableAndColumns(configuration);

                MySQLSessionFactory = configuration.BuildSessionFactory();
            }
            catch (Exception ex)
            { }
        }
    }
}
