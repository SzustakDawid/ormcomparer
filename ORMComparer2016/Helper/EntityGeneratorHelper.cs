﻿using ORMComparer2016.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.Helper
{
    public class EntityGeneratorHelper
    {
        Random random;

        public EntityGeneratorHelper()
        {
            random = new Random();
        }

        public List<EmployeeEntity> GetEmployees(int count)
        {
           var employees = EmployeeEntity.GenerateInstances(count);

            employees.ForEach(x =>
            {
                x.Departments = DepartmentEntity.GenerateExistInstances(random.Next(1, 5));
                x.Departments.ToList().ForEach(z =>
                {
                    z.Id = random.Next(1, 100);
                });
            });

            return employees;
        }
    }
}