﻿using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Infrastructure;
using ORMComparer2016.Infrastructure.Bundles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ORMComparer2016
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            EntityFrameworkContextFactory.ConfigureEntityFramework();
            NHibernateSessionFactory.ConfigureNHibernate(true);
        }
    }
}
