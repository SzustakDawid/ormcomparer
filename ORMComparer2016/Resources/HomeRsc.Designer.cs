﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ORMComparer2016.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class HomeRsc {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal HomeRsc() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ORMComparer2016.Resources.HomeRsc", typeof(HomeRsc).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to O mnie.
        /// </summary>
        public static string AboutLink {
            get {
                return ResourceManager.GetString("AboutLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ORM Comparer 2016.
        /// </summary>
        public static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dawid Szustak.
        /// </summary>
        public static string Author {
            get {
                return ResourceManager.GetString("Author", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Praca Magisterska.
        /// </summary>
        public static string FooterThopic {
            get {
                return ResourceManager.GetString("FooterThopic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Strona Główna.
        /// </summary>
        public static string HomeLink {
            get {
                return ResourceManager.GetString("HomeLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MySQL.
        /// </summary>
        public static string MySQL {
            get {
                return ResourceManager.GetString("MySQL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Testy wydajności ORM.
        /// </summary>
        public static string ORMLink {
            get {
                return ResourceManager.GetString("ORMLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liczba rekordów:.
        /// </summary>
        public static string RecordCount {
            get {
                return ResourceManager.GetString("RecordCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Odśwież.
        /// </summary>
        public static string Refresh {
            get {
                return ResourceManager.GetString("Refresh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wyniki.
        /// </summary>
        public static string Results {
            get {
                return ResourceManager.GetString("Results", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uruchom test.
        /// </summary>
        public static string RunTest {
            get {
                return ResourceManager.GetString("RunTest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SQL Server.
        /// </summary>
        public static string SQLServer {
            get {
                return ResourceManager.GetString("SQLServer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Podsumowanie.
        /// </summary>
        public static string Summary {
            get {
                return ResourceManager.GetString("Summary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liczba testów:.
        /// </summary>
        public static string TestCount {
            get {
                return ResourceManager.GetString("TestCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Testy wydajności usuwania.
        /// </summary>
        public static string TestDeleteBtn {
            get {
                return ResourceManager.GetString("TestDeleteBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Testy wydajności odczytu.
        /// </summary>
        public static string TestReadBtn {
            get {
                return ResourceManager.GetString("TestReadBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Testy wydajności modyfikacji.
        /// </summary>
        public static string TestUpdateBtn {
            get {
                return ResourceManager.GetString("TestUpdateBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Testy wydajności zapisu.
        /// </summary>
        public static string TestWriteBtn {
            get {
                return ResourceManager.GetString("TestWriteBtn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dawid Szustak - ORMComparer 2016.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
    }
}
