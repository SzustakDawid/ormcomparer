﻿using NHibernate.Linq;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.Utils
{
    public static class TestVMFactory
    {
        public static TestVM SetMaximumNumber(TestVM model)
        {
            int sqlServerMaximumNumber = 0;
            int mySQLMaximumNumber = 0;

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                sqlServerMaximumNumber = session.Query<ContractorEntity>().Count();
            }

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                mySQLMaximumNumber = session.Query<ContractorEntity>().Count();
            }

            model.MaximumNumber = Math.Min(9999999, Math.Min(sqlServerMaximumNumber, mySQLMaximumNumber));

            return model;
        }
    }
}