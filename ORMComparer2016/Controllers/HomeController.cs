﻿using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.ViewModel.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ORMComparer2016.Controllers
{
    public partial class HomeController : Controller
    {
        // GET: Home
        public virtual ActionResult Index()
        {
            var model = new HomeVM();

            return View(model);
        }

        public virtual ActionResult RefreshConfiguration()
        {
            EntityFrameworkContextFactory.ConfigureEntityFramework();
            NHibernateSessionFactory.ConfigureNHibernate(true);

            var model = new HomeVM();

            return View("Index", model);
        }
    }
}