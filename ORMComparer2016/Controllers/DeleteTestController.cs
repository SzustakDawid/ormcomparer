﻿using ORMComparer2016.Constants;
using ORMComparer2016.DomainService.Contractor;
using ORMComparer2016.DomainService.Employee;
using ORMComparer2016.Utils;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ORMComparer2016.Controllers
{
    public partial class DeleteTestController : Controller
    {
        public virtual ActionResult Index()
        {
            var model = new TestVM();

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(TestVM model)
        {
            try
            {
                ResultVM result;

                if (model.TestTable == TestTableConst.Contractor)
                {
                    var test = new DeleteContractorTest();
                    result = test.DeleteContractors(model);
                    test.Dispose();
                }
                else
                {
                    var test = new DeleteEmployeeTest();
                    result = test.DeleteEmployees(model);
                    test.Dispose();
                }


                return View(MVC.Shared.Views.Result, result);
            }
            catch (Exception ex)
            {
#if DEBUG
                throw;
#endif 
                return RedirectToAction(HomeController.ActionNameConstants.Index, HomeController.NameConst);
            }
        }
    }
}