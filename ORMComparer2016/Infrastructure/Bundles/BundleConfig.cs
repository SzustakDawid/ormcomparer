﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ORMComparer2016.Infrastructure.Bundles
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            Links.T4MVCExtensions.SetReturnVirtualPath();

            bundles.Add(new ScriptBundle(Links.Scripts.Url())
                .Include(Links.Scripts.jquery_2_2_4_min_js)
                .Include(Links.Scripts.bootstrap_min_js)
                .Include(Links.Scripts.modernizr_2_8_3_min_js));

            bundles.Add(new StyleBundle(Links.Content.Url())
                .Include(Links.Content.Site_css)
                .Include(Links.Content.bootstrap_min_css)
                .Include(Links.Content.bootstrap_theme_min_css));
        }
    }
}