﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Links
{
    public static class T4MVCExtensions
    {
        public static void SetReturnVirtualPath()
        {
            T4MVCHelpers.ProcessVirtualPath = delegate (string virtualPath)
            {
                return virtualPath;
            };
        }
        public static void SetReturnDefaultPath()
        {
            T4MVCHelpers.ProcessVirtualPath = delegate (string virtualPath)
            {
                string path = VirtualPathUtility.ToAbsolute(virtualPath);

                return path;
            };
        }
    }
}