﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.Constants
{
    public class TestKindConst
    {
        public static string Save = "Zapis";

        public static string Update = "Aktualizacja";

        public static string Read = "Odczyt";

        public static string Delete = "Usunięcie";
    }
}