﻿using NHibernate.Linq;
using ORMComparer2016.Constants;
using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ORMComparer2016.DomainService.Contractor
{
    public class ReadContractorTest : IDisposable
    {
        public ResultVM Result { get; set; }

        private Stopwatch _stopWatch;

        public ReadContractorTest()
        {
            Result = new ResultVM();
            Result.TestKind = TestKindConst.Read;
            Result.TestTable = TestTableConst.Contractor;

            _stopWatch = new Stopwatch();
        }

        public ResultVM ReadContractors(TestVM model)
        {
            try
            {
                Result.RecordCount = model.RecordCount;
                Result.TestCount = model.TestCount;

                for (int i = 0; i < model.TestCount; i++)
                {
                    ReadNHibernateSQLServer(model.RecordCount);
                    ReadEntityFrameworkSQLServer(model.RecordCount);

                    ReadNHibernateMySQL(model.RecordCount);
                    ReadEntityFrameworkMySQL(model.RecordCount);
                }

                return Result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ReadEntityFrameworkMySQL(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetMySQLContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Contractors.Take(count).ToList();
                }
            }

            _stopWatch.Stop();

            Result.EntityFrameworkMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void ReadNHibernateMySQL(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<ContractorEntity>().Take(count).ToList();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void ReadEntityFrameworkSQLServer(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetSQLServerContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Contractors.Take(count).ToList();
                }
            }

            _stopWatch.Stop();

            Result.EntityFrameworkSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void ReadNHibernateSQLServer(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<ContractorEntity>().Take(count).ToList();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        public void Dispose()
        {
            Result = null;
            _stopWatch.Reset();
            _stopWatch = null;
        }
    }
}