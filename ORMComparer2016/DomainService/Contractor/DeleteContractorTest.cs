﻿using NHibernate.Linq;
using ORMComparer2016.Constants;
using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ORMComparer2016.DomainService.Contractor
{
    public class DeleteContractorTest : IDisposable
    {
        public ResultVM Result { get; set; }

        public DeleteContractorTest()
        {
            Result = new ResultVM();
            Result.TestKind = TestKindConst.Delete;
            Result.TestTable = TestTableConst.Contractor;

            _stopWatch = new Stopwatch();
        }

        private Stopwatch _stopWatch;

        public ResultVM DeleteContractors(TestVM model)
        {
            try
            {
                Result.RecordCount = model.RecordCount;
                Result.TestCount = model.TestCount;

                for (int i = 0; i < model.TestCount; i++)
                {
                    DeleteNHibernateSQLServer(model.RecordCount);
                    DeleteEntityFrameworkSQLServer(model.RecordCount);

                    DeleteNHibernateMySQL(model.RecordCount);
                    DeleteEntityFrameworkMySQL(model.RecordCount);
                }

                return Result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void DeleteEntityFrameworkMySQL(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetMySQLContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Contractors.Take(count);
                    foreach (var el in list)
                    {
                        context.Contractors.Remove(el);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void DeleteNHibernateMySQL(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<ContractorEntity>().Take(count);
                    foreach(var el in list)
                    {
                        session.Delete(el);
                    }
                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void DeleteEntityFrameworkSQLServer(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetSQLServerContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Contractors.Take(count);
                    foreach(var el in list)
                    {
                        context.Contractors.Remove(el);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void DeleteNHibernateSQLServer(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<ContractorEntity>().Take(count);
                    foreach(var el in list)
                    {
                        session.Delete(el);
                    }
                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        public void Dispose()
        {
            Result = null;
            _stopWatch.Reset();
            _stopWatch = null;
        }
    }
}