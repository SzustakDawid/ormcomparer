﻿using ORMComparer2016.Constants;
using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using System.Web;

namespace ORMComparer2016.DomainService.Contractor
{
    public class InsertContractorTest : IDisposable
    {
        public ResultVM Result { get; set; }

        public InsertContractorTest()
        {
            Result = new ResultVM();
            Result.TestKind = TestKindConst.Save;
            Result.TestTable = TestTableConst.Contractor;

            _stopWatch = new Stopwatch();
        }

        private List<ContractorEntity> _contractorsToSave;

        private Stopwatch _stopWatch;

        public ResultVM InsertContractors(TestVM model)
        {
            try
            {
                Result.RecordCount = model.RecordCount;
                Result.TestCount = model.TestCount;

                for (int i = 0; i < model.TestCount; i++)
                {
                    _contractorsToSave = ContractorEntity.GenerateInstances(model.RecordCount);

                    SaveNHibernateSQLServer();
                    SaveEntityFrameworkSQLServer();

                    SaveNHibernateMySQL();
                    SaveEntityFrameworkMySQL();
                }

                return Result;
            }
            catch (Exception)
            {
                throw;
            }      
        }

        private void SaveNHibernateSQLServer()
        {
            var contractors = ContractorEntity.CloneInstances(_contractorsToSave);

            _stopWatch.Start();

                using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        foreach (var contractor in contractors)
                        {
                            session.Save(contractor);
                        }
                        transaction.Commit();
                    }
                }

            _stopWatch.Stop();

            Result.NHibernateSQLServer.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            contractors.Clear();
            contractors = null;
        }

        private void SaveNHibernateMySQL()
        {
            var contractors = ContractorEntity.CloneInstances(_contractorsToSave);

            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var contractor in contractors)
                    {
                        session.Save(contractor);
                    }
                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateMySQL.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            contractors.Clear();
            contractors = null;
        }

        private void SaveEntityFrameworkSQLServer()
        {
            var contractors = ContractorEntity.CloneInstances(_contractorsToSave);

            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetSQLServerContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var contractor in contractors)
                    {
                        context.Contractors.Add(contractor);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkSQLServer.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            contractors.Clear();
            contractors = null;
        }

        private void SaveEntityFrameworkMySQL()
        {
            var contractors = ContractorEntity.CloneInstances(_contractorsToSave);

            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetMySQLContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var contractor in contractors)
                    {
                        context.Contractors.Add(contractor);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkMySQL.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            contractors.Clear();
            contractors = null;
        }

        public void Dispose()
        {
            Result = null;
            _contractorsToSave.Clear();
            _contractorsToSave = null;
        }
    }
}