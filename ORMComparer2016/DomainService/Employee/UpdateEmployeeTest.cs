﻿using NHibernate.Linq;
using ORMComparer2016.Constants;
using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.Database.Utils;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ORMComparer2016.DomainService.Employee
{
    public class UpdateEmployeeTest : IDisposable
    {
        public ResultVM Result { get; set; }

        private Stopwatch _stopWatch;

        public UpdateEmployeeTest()
        {
            Result = new ResultVM();
            Result.TestKind = TestKindConst.Update;
            Result.TestTable = TestTableConst.Employee;

            _stopWatch = new Stopwatch();
        }

        public ResultVM UpdateEmployees(TestVM model)
        {
            try
            {
                Result.RecordCount = model.RecordCount;
                Result.TestCount = model.TestCount;

                for (int i = 0; i < model.TestCount; i++)
                {
                    UpdateNHibernateSQLServer(model.RecordCount);
                    UpdateEntityFrameworkSQLServer(model.RecordCount);

                    UpdateNHibernateMySQL(model.RecordCount);
                    UpdateEntityFrameworkMySQL(model.RecordCount);
                }

                return Result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void UpdateEntityFrameworkMySQL(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetMySQLContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Employees.Take(count).ToList();

                    list.ForEach(x => x.FirstName = "EF Update");

                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void UpdateNHibernateMySQL(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<EmployeeEntity>().Take(count).ToList();

                    list.ForEach(x =>
                    {
                        x.FirstName = "NHibernate Update";
                        session.Update(x);
                    });

                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateMySQL.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void UpdateEntityFrameworkSQLServer(int count)
        {
            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetSQLServerContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var list = context.Employees.Take(count).ToList();

                    list.ForEach(x => x.FirstName = "EF Update");

                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        private void UpdateNHibernateSQLServer(int count)
        {
            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var list = session.Query<EmployeeEntity>().Take(count).ToList();

                    list.ForEach(x =>
                    {
                        x.FirstName = "NHibernate Update";
                        session.Update(x);
                    });

                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateSQLServer.Add(_stopWatch.Elapsed);
            _stopWatch.Reset();
        }

        public void Dispose()
        {
            Result = null;
            _stopWatch.Reset();
            _stopWatch = null;
        }
    }
}