﻿using ORMComparer2016.Constants;
using ORMComparer2016.Database.Config.Entity_Framework;
using ORMComparer2016.Database.Config.NHibernate;
using ORMComparer2016.Database.Entity;
using ORMComparer2016.Helper;
using ORMComparer2016.ViewModel.Result;
using ORMComparer2016.ViewModel.Test;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ORMComparer2016.DomainService.Employee
{
    public class InsertEmployeeTest : IDisposable
    {
        public ResultVM Result { get; set; }

        EntityGeneratorHelper egh = new EntityGeneratorHelper();

        public InsertEmployeeTest()
        {
            Result = new ResultVM();
            Result.TestKind = TestKindConst.Save;
            Result.TestTable = TestTableConst.Employee;

            _stopWatch = new Stopwatch();
        }

        private List<EmployeeEntity> _employeeToSave;

        private Stopwatch _stopWatch;

        public ResultVM InsertEmployees(TestVM model)
        {
            try
            {
                Result.RecordCount = model.RecordCount;
                Result.TestCount = model.TestCount;

                for (int i = 0; i < model.TestCount; i++)
                {
                    _employeeToSave = egh.GetEmployees(model.RecordCount);

                    SaveNHibernateSQLServer();
                    SaveEntityFrameworkSQLServer();

                    SaveNHibernateMySQL();
                    SaveEntityFrameworkMySQL();
                }

                return Result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SaveNHibernateSQLServer()
        {
            var employees = EmployeeEntity.CloneInstances(_employeeToSave);

            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.SQLServerSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var employee in employees)
                    {
                        session.Save(employee);
                    }
                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateSQLServer.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            employees.Clear();
            employees = null;
        }

        private void SaveNHibernateMySQL()
        {
            var employees = EmployeeEntity.CloneInstances(_employeeToSave);

            _stopWatch.Start();

            using (var session = NHibernateSessionFactory.MySQLSessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var employee in employees)
                    {
                        session.Save(employee);
                    }
                    transaction.Commit();
                }
            }

            _stopWatch.Stop();

            Result.NHibernateMySQL.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            employees.Clear();
            employees = null;
        }

        private void SaveEntityFrameworkSQLServer()
        {
            var employees = EmployeeEntity.CloneInstances(_employeeToSave);

            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetSQLServerContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var employee in employees)
                    {
                        _stopWatch.Stop();
                        var departments = new List<DepartmentEntity>();
                        foreach (var dep in employee.Departments)
                            departments.Add(context.Departments.Single(x => x.Id == dep.Id));
                        employee.Departments = departments;
                        _stopWatch.Start();

                        context.Employees.Add(employee);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkSQLServer.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            employees.Clear();
            employees = null;
        }

        private void SaveEntityFrameworkMySQL()
        {
            var employees = EmployeeEntity.CloneInstances(_employeeToSave);

            _stopWatch.Start();

            using (var context = EntityFrameworkContextFactory.GetMySQLContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var employee in employees)
                    {
                        _stopWatch.Stop();
                        var departments = new List<DepartmentEntity>();
                        foreach (var dep in employee.Departments)
                            departments.Add(context.Departments.Single(x => x.Id == dep.Id));
                        employee.Departments = departments;
                        _stopWatch.Start();

                        context.Employees.Add(employee);
                    }
                    transaction.Commit();
                }
                context.SaveChanges();
            }

            _stopWatch.Stop();

            Result.EntityFrameworkMySQL.Add(_stopWatch.Elapsed);

            _stopWatch.Reset();
            employees.Clear();
            employees = null;
        }

        public void Dispose()
        {
            Result = null;
            _employeeToSave.Clear();
            _employeeToSave = null;
        }
    }
}