﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.ViewModel.Test
{
    public class TestVM
    {
        public int RecordCount { get; set; }

        public int TestCount { get; set; }

        public int MaximumNumber { get; set; }

        public string TestTable { get; set; }
    }
}