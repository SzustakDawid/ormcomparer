﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.ViewModel.Result
{
    public class ResultVM
    {
        public ResultVM()
        {
            NHibernateSQLServer = new List<TimeSpan>();
            EntityFrameworkSQLServer = new List<TimeSpan>();
            NHibernateMySQL = new List<TimeSpan>();
            EntityFrameworkMySQL = new List<TimeSpan>();
        }

        public List<TimeSpan> NHibernateSQLServer { get; set; }

        public List<TimeSpan> EntityFrameworkSQLServer { get; set; }

        public List<TimeSpan> NHibernateMySQL { get; set; }

        public List<TimeSpan> EntityFrameworkMySQL { get; set; }

        public TimeSpan NHibernateSQLServerAverage
        {
            get
            {
                if (NHibernateSQLServer != null && NHibernateSQLServer.Count > 0)
                {
                    return new TimeSpan(NHibernateSQLServer.Sum(x => x.Ticks) / NHibernateSQLServer.Count);
                }

                return new TimeSpan();
            }
        }

        public TimeSpan EntityFrameworkSQLServerAverage
        {
            get
            {
                if (EntityFrameworkSQLServer != null && EntityFrameworkSQLServer.Count > 0)
                {
                    return new TimeSpan(EntityFrameworkSQLServer.Sum(x => x.Ticks) / EntityFrameworkSQLServer.Count);
                }

                return new TimeSpan();
            }
        }

        public TimeSpan NHibernateMySQLAverage
        {
            get
            {
                if (NHibernateMySQL != null && NHibernateMySQL.Count > 0)
                {
                    return new TimeSpan(NHibernateMySQL.Sum(x => x.Ticks) / NHibernateMySQL.Count);
                }

                return new TimeSpan();
            }
        }

        public TimeSpan EntityFrameworkMySQLAverage
        {
            get
            {
                if (EntityFrameworkMySQL != null && EntityFrameworkMySQL.Count > 0)
                {
                    return new TimeSpan(EntityFrameworkMySQL.Sum(x => x.Ticks) / EntityFrameworkMySQL.Count);
                }

                return new TimeSpan();
            }
        }

        public string TestKind { get; set; }

        public int RecordCount { get; set; }

        public int TestCount { get; set; }

        public string TestTable { get; set; }
    }
}