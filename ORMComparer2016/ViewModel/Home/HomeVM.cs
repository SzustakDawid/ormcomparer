﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORMComparer2016.ViewModel.Home
{
    public class HomeVM
    {
        public bool IsNHibernateMySQLConnection
        {
            get
            {
                return ORMComparer2016.Database.Config.NHibernate.NHibernateSessionFactory.MySQLSessionFactory != null;
            }
        }

        public bool IsNHibernateSQLServerConnection
        {
            get
            {
                return ORMComparer2016.Database.Config.NHibernate.NHibernateSessionFactory.SQLServerSessionFactory != null;
            }
        }

        public bool IsEntityFrameworkMySQLConnection
        {
            get
            {
                return ORMComparer2016.Database.Config.Entity_Framework.EntityFrameworkContextFactory.IsMySQLConnectionCorrect;
            }
        }

        public bool IsEntityFrameworkSQLServerConnection
        {
            get
            {
                return ORMComparer2016.Database.Config.Entity_Framework.EntityFrameworkContextFactory.IsSQLServerConnectionCorrect;
            }
        }

        public string NHibernateMySQLConnectionStatus
        {
            get
            {
                return GetStatusByFlag(IsNHibernateMySQLConnection);
            }
        }

        public string NHibernateSQLServerConnectionStatus
        {
            get
            {
                return GetStatusByFlag(IsNHibernateSQLServerConnection);
            }
        }

        public string EntityFrameworkMySQLConnectionStatus
        {
            get
            {
                return GetStatusByFlag(IsEntityFrameworkMySQLConnection);
            }
        }
        public string EntityFrameworkSQLServerConnectionStatus
        {
            get
            {
                return GetStatusByFlag(IsEntityFrameworkSQLServerConnection);
            }
        }

        private string GetStatusByFlag(bool flag)
        {
            string result = "Brak";
            if (flag)
                result = "OK";

            return result;
        }
    }
}