# ORMComparer #
Aplikacja **ASP .NET MVC** wykonana w ramach pracy magisterskiej, służąca do porównania wydajności NHibernate oraz Entity Framework na dwóch silnikach zarządzania bazami danych MySQL oraz MS SQL Server.  
#  #
**Screeny aplikacji:**  
![1.png](https://bitbucket.org/repo/LorqxEK/images/125914422-1.png)  
#  #
![2.png](https://bitbucket.org/repo/LorqxEK/images/3276754457-2.png)  
#  #
![33.png](https://bitbucket.org/repo/LorqxEK/images/2112235046-33.png)  
#  #
![4.png](https://bitbucket.org/repo/LorqxEK/images/2640994482-4.png)